﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xero.Api.Core;
using Xero.Api.Example.Applications.Private;
using Xero.Api.Infrastructure.OAuth;
using static xi.Helper.ExtensionMethods;
using static xi.Helper.Enums;
using Xero.Api.Serialization;
using elysium.crypt;
using Xero.Api.Core.Model;
using Xero.Api.Core.Model.Status;
using Xero.Api.Core.Model.Types;
using Xero.Api.Infrastructure.Model;
using Xero.Api.Infrastructure.Exceptions;

namespace xi.xero
{
    public class xeroWrapper
    {
        public string DecryptedConsumerKey { get; private set; }
        public string DecryptedConsumerSecret { get; private set; }

        private XeroCoreApi xeroApi;


        #region Documentation -----------------------------------------------------------------------------
        /// <summary>   Class Constructor. </summary>
        ///
        /// <remarks>   
        ///             Dirk Strauss, 2017/12/19. 
        ///             Instantiates a new instance of the XeroCoreApi for the given Application Type (Currently only Private Application Types supported).
        /// </remarks>
        ///
        /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
        ///
        /// <param name="encryptedConsumerKey">         The encrypted consumer key. </param>
        /// <param name="encryptedConsumerSecret">      The encrypted consumer secret. </param>
        /// <param name="encryptedCertificatePath">     Full pathname of the encrypted certificate file. </param>
        /// <param name="encryptedCertificatePassword"> The encrypted certificate password. </param>
        /// <param name="plainTextBaseUri">             Plain text base URI. </param>
        /// <param name="appType">                      The Application Type (Public, Private, Partner). </param>
        #endregion
        public xeroWrapper(string encryptedConsumerKey, string encryptedConsumerSecret, string encryptedCertificatePath, string encryptedCertificatePassword, string plainTextBaseUri, APIApplicationType appType)
        {
            try
            {
                var user = new ApiUser { Name = Environment.MachineName };
                DecryptedConsumerKey = crypto.RijndaelDecrypt(encryptedConsumerKey);
                DecryptedConsumerSecret = crypto.RijndaelDecrypt(encryptedConsumerSecret);
                var consumer = new Consumer(DecryptedConsumerKey, DecryptedConsumerSecret);

                if (appType == APIApplicationType.Private)
                {
                    var privateAuth = new PrivateAuthenticator(crypto.RijndaelDecrypt(encryptedCertificatePath), crypto.RijndaelDecrypt(encryptedCertificatePassword));
                    xeroApi = new XeroCoreApi(plainTextBaseUri, privateAuth, consumer, user, new DefaultMapper(), new DefaultMapper());
                }
                else
                    throw new Exception("The Xero Wrapper currently only supports Private Application Types");
            }
            catch (Exception ex) when (!XeroLogging.ExceptionLogged)
            {
                ex.Data.Add("APIApplicationType", appType.ToString());
                ex.Data.Add("XeroCoreApi_IsNull", xeroApi.IsNull());
                ex.Data.Add("BaseUri", plainTextBaseUri);
                XeroLogging.LogExceptions(ex);
                throw;
            }
        }


        #region Contacts (Find)

        #region FindContactPage

        #region Documentation -----------------------------------------------------------------------------
        /// <summary>   Get a single contact page. </summary>
        ///
        /// <remarks>   Dirk Strauss, 2017/12/19. </remarks>
        ///
        /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
        ///
        /// <param name="page"> The page number. </param>
        ///
        /// <returns>   A List of Contact objects. </returns>
        #endregion
        public List<Contact> FindContactPage(int page)
        {
            List<Contact> contactList = new List<Contact>();

            try
            {
                if (!(xeroApi.IsNull()))
                {
                    var contactPage = xeroApi.Contacts.Page(page);
                    contactList = (List<Contact>)contactPage.Find();
                }
                else
                    throw new Exception("XeroCoreApi not instantiated");
            }
            catch (Exception ex) when (!XeroLogging.ExceptionLogged)
            {
                ex.Data.Add("XeroCoreApi_IsNull", xeroApi.IsNull());
                ex.Data.Add("Page_Requested", page);
                XeroLogging.LogExceptions(ex);
                throw;
            }

            return contactList;
        }
        #endregion

        #region FindAllContacts

        #region Documentation -----------------------------------------------------------------------------
        /// <summary>   Gets all contacts. </summary>
        ///
        /// <remarks>   Dirk Strauss, 2017/12/19. </remarks>
        ///
        /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
        ///
        /// <returns>   All contacts in the Xero Company. </returns>
        #endregion
        public List<Contact> FindAllContacts()
        {
            List<Contact> contactList = new List<Contact>();
            List<Contact> contactPage = new List<Contact>();
            int pageNumber = 1;

            try
            {
                if (!(xeroApi.IsNull()))
                {
                    do
                    {
                        contactPage = xeroApi.Contacts.Page(pageNumber).Find().ToList();
                        if (contactPage.Count > 0)
                            contactList.AddRange(contactPage);

                        pageNumber += 1;
                    } while (contactPage.Count > 0);

                }
                else
                    throw new Exception("XeroCoreApi not instantiated");
            }
            catch (Exception ex) when (!XeroLogging.ExceptionLogged)
            {
                ex.Data.Add("XeroCoreApi_IsNull", xeroApi.IsNull());
                ex.Data.Add("LastPageNumberProcessed", pageNumber);
                ex.Data.Add($"{nameof(Contact)}sFound", contactList.Count);
                XeroLogging.LogExceptions(ex);
                throw;
            }

            return contactList;
        }
        #endregion

        #region FindContact

        #region Documentation -----------------------------------------------------------------------------
        /// <summary>   Gets a contact based on the Contact ID string. </summary>
        ///
        /// <remarks>   Dirk Strauss, 2017/12/19. </remarks>
        ///
        /// <param name="contactId">    Identifier for the contact. </param>
        ///
        /// <returns>   The contact with the specific ID. </returns>
        #endregion
        public Contact FindContact(string contactId)
        {
            return FindContact(new Guid(contactId));
        }


        #region Documentation -----------------------------------------------------------------------------
        /// <summary>   Gets a contact based on the Contact ID GUID. </summary>
        ///
        /// <remarks>   Dirk Strauss, 2017/12/19. </remarks>
        ///
        /// <param name="contactId">    Identifier for the contact. </param>
        ///
        /// <returns>   The contact with the specific ID. </returns>
        #endregion
        public Contact FindContact(Guid contactId)
        {
            Contact contact = new Contact();
            List<Contact> contactList = new List<Contact>();

            try
            {
                contactList = FindAllContacts();
                if (contactList.Count > 0)
                    contact = contactList.Where(c => c.Id.Equals(contactId)).FirstOrDefault();
            }
            catch (Exception ex) when (!XeroLogging.ExceptionLogged)
            {
                ex.Data.Add("ContactID", contactId);
                ex.Data.Add($"{nameof(Contact)}sFound", contactList.Count);
                XeroLogging.LogExceptions(ex);
                throw;
            }

            return contact;
        }
        #endregion

        #endregion

        #region Accounts (Find)

        #region FindAllBankAccounts

        #region Documentation -----------------------------------------------------------------------------
        /// <summary>   Gets all accounts for the specific BankAccountType. </summary>
        ///
        /// <remarks>   Dirk Strauss, 2017/12/20. </remarks>
        ///
        /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
        ///
        /// <param name="bankType"> Type of the bank. </param>
        ///
        /// <returns>   A list of all accounts for the specific BankAccountType. </returns>
        #endregion
        public List<Account> FindAllBankAccounts(BankAccountType bankType)
        {
            List<Account> accountList = new List<Account>();
            string bankTypeToFind = Enum.GetName(typeof(BankAccountType), bankType.ToInt());

            try
            {
                if (!(xeroApi.IsNull()))
                {
                    accountList = (from p in xeroApi.Accounts.Find().ToList()
                                   where !string.IsNullOrEmpty(p.BankAccountType.ToString())
                                   && p.BankAccountType.ToString() == bankTypeToFind
                                   select p).ToList();
                }
                else
                    throw new Exception("XeroCoreApi not instantiated");
            }
            catch (Exception ex) when (!XeroLogging.ExceptionLogged)
            {
                ex.Data.Add("XeroCoreApi_IsNull", xeroApi.IsNull());
                ex.Data.Add("BankAccountType_Enum", bankTypeToFind);
                ex.Data.Add($"{nameof(Account)}sFound", accountList.Count);
                XeroLogging.LogExceptions(ex);
                throw;
            }

            return accountList;
        }
        #endregion

        #endregion

        #region BankTransactions (Find)

        #region FindBankStatement

        #region Documentation -----------------------------------------------------------------------------
        /// <summary>   Gets the bank statement for the specific Account ID (GUID). </summary>
        ///
        /// <remarks>   Dirk Strauss, 2017/12/20. </remarks>
        ///
        /// <param name="accountIdGuid">    Unique identifier for the account. </param>
        /// <param name="from">             (Optional) From date. </param>
        /// <param name="to">               (Optional) To date. </param>
        ///
        /// <returns>   A List of Bank Transactions. </returns>
        #endregion
        public List<BankTransaction> FindBankStatement(string accountIdGuid, DateTime? from = null, DateTime? to = null)
        {
            return FindBankStatement(new Guid(accountIdGuid), from, to);
        }

        #region Documentation -----------------------------------------------------------------------------
        /// <summary>   Gets the bank statement for the specific Account ID (GUID). </summary>
        ///
        /// <remarks>   Dirk Strauss, 2017/12/20. </remarks>
        ///
        /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
        ///
        /// <param name="accountIdGuid">    Unique identifier for the account. </param>
        /// <param name="from">             (Optional) From date. </param>
        /// <param name="to">               (Optional) To date. </param>
        ///
        /// <returns>   A List of Bank Transactions. </returns>
        #endregion
        public List<BankTransaction> FindBankStatement(Guid accountIdGuid, DateTime? from = null, DateTime? to = null)
        {
            List<BankTransaction> transactionsList = new List<BankTransaction>();
            List<BankTransaction> transactionPage = new List<BankTransaction>();
            int pageNumber = 1;

            try
            {
                if (!(xeroApi.IsNull()))
                {
                    do
                    {
                        xeroApi.Reports.BankStatement(accountIdGuid, from, to);
                        transactionPage = xeroApi.BankTransactions.Page(pageNumber).Find().ToList();
                        if (transactionPage.Count > 0)
                            transactionsList.AddRange(transactionPage);

                        pageNumber += 1;
                    } while (transactionPage.Count > 0);

                }
                else
                    throw new Exception("XeroCoreApi not instantiated");
            }
            catch (Exception ex) when (!XeroLogging.ExceptionLogged)
            {
                ex.Data.Add("XeroCoreApi_IsNull", xeroApi.IsNull());
                ex.Data.Add("LastPageNumberProcessed", pageNumber);
                ex.Data.Add("accountIdGuid", accountIdGuid);
                ex.Data.Add("From_date", from);
                ex.Data.Add("To_date", to);
                ex.Data.Add($"{nameof(BankTransaction)}sFound", transactionsList.Count);
                XeroLogging.LogExceptions(ex);
                throw;
            }

            return transactionsList;
        }
        #endregion

        #endregion

        #region Invoices (Find)

        #region FindInvoice

        #region Documentation -----------------------------------------------------------------------------
        /// <summary>   Searches for a specific invoice for the given Invoice Number. </summary>
        ///
        /// <remarks>   Dirk Strauss, 2017/12/20. </remarks>
        ///
        /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
        ///
        /// <param name="invoiceNumber">    The invoice number. </param>
        ///
        /// <returns>   The found invoice. </returns>
        #endregion
        public Invoice FindInvoice(string invoiceNumber)
        {
            Invoice invoice = new Invoice();

            try
            {
                if (!(xeroApi.IsNull()))
                {
                    invoice = xeroApi.Invoices.Find().Where(i => i.Number.Equals(invoiceNumber)).FirstOrDefault();
                }
                else
                    throw new Exception("XeroCoreApi not instantiated");
            }
            catch (Exception ex) when (!XeroLogging.ExceptionLogged)
            {
                ex.Data.Add("XeroCoreApi_IsNull", xeroApi.IsNull());
                ex.Data.Add("InvoiceToFind", invoiceNumber);
                XeroLogging.LogExceptions(ex);
                throw;
            }

            return invoice;
        }
        #endregion 
        #endregion

        #region Documentation -----------------------------------------------------------------------------
        /// <summary>   Creates an invoice. </summary>
        ///
        /// <remarks>   Dirk Strauss, 2017/12/21. </remarks>
        ///
        /// <exception cref="Exception">
        ///     Thrown when an exception error condition occurs.
        /// </exception>
        /// <exception cref="ValidationException">
        ///     Thrown when a Validation error condition occurs.
        /// </exception>
        ///
        /// <param name="invoiceNumber">        The invoice number. </param>
        /// <param name="reference">            The reference. </param>
        /// <param name="currencyCode">         The currency code. </param>
        /// <param name="url">                  Url must be a valid absolute url. </param>
        /// <param name="lineAmountType">       Define is the line amount is Inclusive, Exclusive or NoTax. </param>
        /// <param name="invoiceStatus">        The invoice status. </param>
        /// <param name="invoiceType">          Type of the invoice. </param>
        /// <param name="contact">              The Contact must contain at least 1 of the following elements to identify the contact: Name, ContactID, ContactNumber. </param>
        /// <param name="invoiceID">            Identifier for the invoice. </param>
        /// <param name="prepayments">          The prepayments. </param>
        /// <param name="creditNotes">          The credit notes. </param>
        /// <param name="lineItems">            The line items must be existing inventory items in Xero. </param>
        /// <param name="overpayments">         The overpayments. </param>
        /// <param name="payments">             The payments. </param>
        /// <param name="dueDate">              The document DueDate field must be specified. </param>
        /// <param name="totalDiscount">        (Optional) The total discount. </param>
        /// <param name="total">                (Optional) Number of. </param>
        /// <param name="totalTax">             (Optional) The total tax. </param>
        /// <param name="subTotal">             (Optional) The sub total. </param>
        /// <param name="plannedPaymentDate">   (Optional) The planned payment date. </param>
        /// <param name="expectedPaymentDate">  (Optional) The expected payment date. </param>
        /// <param name="date">                 (Optional) The date. </param>
        /// <param name="currencyRate">         (Optional) The currency rate. </param>
        /// <param name="fullyPaidOnDate">      (Optional) The fully paid on date. </param>
        /// <param name="amountDue">            (Optional) The amount due. </param>
        /// <param name="amountPaid">           (Optional) The amount paid. </param>
        /// <param name="amountCredited">       (Optional) The amount credited. </param>
        /// <param name="hasAttachments">       (Optional) The has attachments. </param>
        /// <param name="sentToContact">        (Optional) The sent to contact. </param>
        /// <param name="brandingThemeId">      (Optional) Identifier for the branding theme. </param>
        ///
        /// <returns>   True if Invoice Creation succeeds, false if it fails. </returns>
        #endregion
        public bool CreateInvoice(string invoiceNumber, string reference, string currencyCode, string url, LineAmountType lineAmountType
            , InvoiceStatus invoiceStatus, InvoiceType invoiceType, Contact contact, List<Prepayment> prepayments
            , List<CreditNote> creditNotes, List<LineItem> lineItems, List<Overpayment> overpayments, List<Payment> payments, DateTime? dueDate
            , decimal? total = null, decimal? totalDiscount = null, decimal? totalTax = null, decimal? subTotal = null
            , DateTime? plannedPaymentDate = null, DateTime? expectedPaymentDate = null, DateTime? date = null
            , decimal? currencyRate = null, DateTime? fullyPaidOnDate = null, decimal? amountDue = null, decimal? amountPaid = null
            , decimal? amountCredited = null, bool? hasAttachments = null, bool? sentToContact = null, Guid? brandingThemeId = null)
        {
            bool createInvoiceCompleted = false;
            Invoice invoice = new Invoice();
            Guid invoiceID = new Guid();
            
            try
            {
                if (!(xeroApi.IsNull()))
                {
                    invoice.Number = invoiceNumber;
                    invoice.Reference = reference;
                    invoice.CurrencyCode = currencyCode;
                    invoice.Prepayments = prepayments;
                    invoice.CreditNotes = creditNotes;
                    invoice.SentToContact = sentToContact;
                    invoice.LineItems = lineItems;
                    invoice.Url = url;
                    invoice.BrandingThemeId = brandingThemeId;
                    invoice.HasAttachments = hasAttachments;
                    invoice.AmountCredited = amountCredited;
                    invoice.AmountPaid = amountPaid;
                    invoice.AmountDue = amountDue;
                    invoice.FullyPaidOnDate = fullyPaidOnDate;
                    invoice.CurrencyRate = currencyRate;
                    invoice.TotalDiscount = totalDiscount;
                    invoice.Total = total;
                    invoice.TotalTax = totalTax;
                    invoice.SubTotal = subTotal;
                    invoice.PlannedPaymentDate = plannedPaymentDate;
                    invoice.ExpectedPaymentDate = expectedPaymentDate;
                    invoice.DueDate = dueDate;
                    invoice.Date = date;
                    invoice.LineAmountTypes = lineAmountType;
                    invoice.Status = invoiceStatus;
                    invoice.Type = invoiceType;
                    invoice.Contact = contact;
                    invoice.Id = invoiceID;
                    invoice.Overpayments = overpayments;
                    invoice.Payments = payments;

                    xeroApi.Create(invoice);
                    createInvoiceCompleted = true;
                }
                else
                    throw new Exception("XeroCoreApi not instantiated");
            }
            catch (ValidationException ex) when (!XeroLogging.ExceptionLogged)
            {
                List<ValidationError> lstErrors = ex.ValidationErrors;
                ex.Data.Add("XeroCoreApi_IsNull", xeroApi.IsNull());
                ex.Data.Add("InvoiceJson", invoice.ToJson());
                ex.Data.Add("ValidationError", lstErrors.ToJson());
                XeroLogging.LogExceptions(ex);
                throw;
            }
            catch (Exception ex) when (!XeroLogging.ExceptionLogged)
            {
                ex.Data.Add("XeroCoreApi_IsNull", xeroApi.IsNull());
                ex.Data.Add("InvoiceJson", invoice.ToJson());
                XeroLogging.LogExceptions(ex);
                throw;
            }

            return createInvoiceCompleted;
        }






        public void CreatePayment(Account account, decimal? paymentAmount)
        {

        }




        #region Documentation -----------------------------------------------------------------------------
        /// <summary>   Creates a payment against an invoice. </summary>
        ///
        /// <remarks>   Dirk Strauss, 2017/12/20. </remarks>
        ///
        /// <exception cref="Exception">
        ///     Thrown when an exception error condition occurs.
        /// </exception>
        /// <exception cref="ValidationException">
        ///     Thrown when a Validation error condition occurs.
        /// </exception>
        ///
        /// <param name="account">          This account needs to be either an account of type BANK or have enable payments to this accounts switched on (see GET Accounts). 
        ///                                 See the edit account screen of your Chart of Accounts in Xero if you wish to enable payments for an account other than a bank account. </param>
        /// <param name="paymentAmount">    The payment amount. </param>
        /// <param name="paymentReference"> The payment reference. </param>
        /// <param name="paymentStatus">    The payment status. </param>
        /// <param name="paymentType">      Type of the payment. </param>
        /// <param name="invoice">          The invoice. </param>
        /// <param name="isReconciled">     True if this payment is reconciled. </param>
        /// <param name="paymentDate">      The payment date. </param>
        ///
        /// <returns>   True if payment succeeds, false if it fails. </returns>
        #endregion
        public bool CreatePayment(Account account, decimal? paymentAmount, string paymentReference, PaymentStatus paymentStatus, PaymentType paymentType, Invoice invoice, bool isReconciled, DateTime paymentDate)
        {
            bool createPaymentCompleted = false;
            Payment pmt = new Payment();
            Guid paymentId = Guid.NewGuid();

            try
            {
                if (!(xeroApi.IsNull()))
                {
                    pmt.Account = account;
                    pmt.Amount = paymentAmount;
                    pmt.Date = paymentDate;
                    pmt.Id = paymentId;
                    pmt.Invoice = invoice;
                    pmt.IsReconciled = isReconciled;
                    pmt.Reference = paymentReference;
                    pmt.Status = paymentStatus;
                    pmt.Type = paymentType;

                    xeroApi.Create(pmt);
                    createPaymentCompleted = true;
                }
                else
                    throw new Exception("XeroCoreApi not instantiated");
            }
            catch (ValidationException ex) when (!XeroLogging.ExceptionLogged)
            {
                List<ValidationError> lstErrors = ex.ValidationErrors;
                ex.Data.Add("XeroCoreApi_IsNull", xeroApi.IsNull());
                ex.Data.Add("PaymentJson", pmt.ToJson());
                ex.Data.Add("ValidationError", lstErrors.ToJson());
                XeroLogging.LogExceptions(ex);
                throw;
            }
            catch (Exception ex) when (!XeroLogging.ExceptionLogged)
            {
                ex.Data.Add("XeroCoreApi_IsNull", xeroApi.IsNull());
                ex.Data.Add("PaymentJson", pmt.ToJson());
                XeroLogging.LogExceptions(ex);
                throw;
            }

            return createPaymentCompleted;
        }


    }


    internal static class XeroLogging
    {
        internal static bool ExceptionLogged { get; private set; } = false;
        internal static void LogExceptions(Exception exception)
        {
            // Placeholder for Embrace Logging class logic
            // TODO: Add logging logic

            // Indicate to the calling code that an exception has been caught and Handled/Logged
            ExceptionLogged = true;
        }
    }







}
