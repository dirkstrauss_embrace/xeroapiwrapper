﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xi.Helper
{
    public static class ExtensionMethods
    {
        #region ToInt
        /// <summary>
        /// Convert a string representation of an integer to an actual integer. Returns -99 if not an integer.
        /// </summary>
        /// <param name="value">String Value</param>
        /// <returns>Integer</returns>
        public static int ToInt(this string value)
        {
            int integerValue = -99;
            if (int.TryParse(value, out integerValue))
            {

            }
            return integerValue;
        }

        /// <summary>
        /// Convert an object to an actual integer. Returns -99 if not an integer.
        /// </summary>
        /// <param name="value">Object</param>
        /// <returns>Integer</returns>
        public static int ToInt(this object value)
        {
            int integerValue = -99;

            if (int.TryParse(value.ToString(), out integerValue))
            {

            }

            return integerValue;
        }
        #endregion

        #region IsNull
        /// <summary>
        /// Check if object is null
        /// </summary>
        /// <typeparam name="T">Check if a class object is null</typeparam>
        /// <param name="obj">The object of type T to act on</param>
        /// <returns>Boolean true or false</returns>
        public static bool IsNull<T>(this T obj) where T : class
        {
            return obj == null;
        }
        #endregion

        #region Create Class of type T from JSON string
        /// <summary>
        /// Create a class object (e.g. myJsonClass) from a JSON string variable called jsonString. Usage:
        /// myJsonClass myjsonObj = jsonString.FromJson<myJsonClass>();
        /// </summary>
        /// <typeparam name="T">The class type to create</typeparam>
        /// <param name="value">the JSON string value</param>
        /// <returns>A Class of type T</returns>
        public static T FromJson<T>(this string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }
        #endregion

        #region Create JSON string from Class of type T
        /// <summary>
        /// Create a JSON String from a class object. Usage:
        /// myJsonClass cc = new myJsonClass();
        /// cc.user = "Mike Miller";
        /// cc.entity_type = "user";
        /// cc.datetime = DateTime.Now; 
        /// string jsonString = cc.ToJson();
        /// </summary>
        /// <typeparam name="T">The class type</typeparam>
        /// <param name="value">The class object</param>
        /// <returns>JSON string of object</returns>
        public static string ToJson<T>(this T value) where T : class
        {
            return JsonConvert.SerializeObject(value, Formatting.Indented);
        } 
        #endregion


        

    }
}
