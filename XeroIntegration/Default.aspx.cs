﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Xero.Api.Core.Model;
using Xero.Api.Core.Model.Status;
using Xero.Api.Core.Model.Types;
using xi.xero;
using static xi.Helper.Enums;

namespace XeroIntegration
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                /*
                -----------------------------------------------------------------
                Encrypted settings in web.config 
                -----------------------------------------------------------------
                App name: EmbraceIntegration
                Application URL: http://www.thisisembrace.com

                key: POO7X1XCJBRCP5BINNYVMOLLXYRGLL
                secret: FHA5ZCMKYXCCVLDADXHLXPULT6TDIR
                certificate path: D:\dev\certs\xero\public_privatekey.pfx
                certificate password: password

                The certificates are also contained in the solution in the []docs folder
                -----------------------------------------------------------------
                */


                xeroWrapper xero = new xeroWrapper
                    (
                    Settings.EncryptedKey
                    , Settings.EncryptedSecret
                    , Settings.EncryptedCertificatePath
                    , Settings.EncryptedCertificatePassword
                    , Settings.BaseUri, APIApplicationType.Private
                    );

                #region Get Contacts

                //xero.GetContactPage(1);

                //xero.GetContact("162c177a-3391-4742-890f-f3ebfe0df1f3"); 
                #endregion

                #region Find Bank Transactions for a specific Bank Account

                /*
                        // Find Bank Transactions for a specific Bank Account
                        var accounts = xero.FindAllBankAccounts(BankAccountType.Bank);

                        string bankTransToFind = "Business Bank Account";
                        Guid accountId = new Guid();
                        foreach (Account acc in accounts)
                        {
                            if (acc.Name.Equals(bankTransToFind))
                            {
                                accountId = acc.Id;
                            }
                        }

                        List<BankTransaction> transactions = xero.FindBankStatement(accountId);
                        */
                #endregion

                #region Pay an Invoice

                /*
                        // Pay an Invoice
                        var invoiceToPay = xero.FindInvoice("INV-0031");

                        string businessSavAcc = "Business Savings Account";
                        foreach (Account acc in accounts)
                        {
                            if (acc.Name.Equals(businessSavAcc))
                            {
                                xero.CreatePayment(acc, 95.00M, "TEST", Xero.Api.Core.Model.Status.PaymentStatus.Authorised, PaymentType.AccountsReceivable, invoiceToPay, false, DateTime.Now);
                            }
                        }
                        */
                #endregion

                
                // Must be existing contact
                Contact contact = new Contact();
                contact.Name = "Star Wors"; // Must be existing contact
                contact.FirstName = "Dirk";
                contact.LastName = "Strauss";
                contact.IsCustomer = true;
                contact.EmailAddress = "dirk.strauss@thisisembrace.com";

                List<Prepayment> prepayments = new List<Prepayment>();
                List<CreditNote> creditNotes = new List<CreditNote>();

                // The items must exist in the Inventory on Xero
                List<LineItem> lineItems = new List<LineItem>();
                LineItem item = new LineItem();
                item.ItemCode = "SPI001";
                item.LineAmount = 500.00m;
                item.Quantity = 1;
                item.Description = "Biltong Spice (a 10kg bag)";
                lineItems.Add(item);

                List<Overpayment> overpayments = new List<Overpayment>();
                List<Payment> payments = new List<Payment>();
                decimal? total = 500.00m;
                decimal? totalDiscount = 0.00m;
                decimal? totalTax = null; // Want to see if Xero calculates the tax
                DateTime dueDate = DateTime.Now.AddDays(15.00);
                
                var blnInvoiceCreated = xero.CreateInvoice
                    (
                    "INV-0032"
                    , "XeroApiTest"
                    , nameof(CurrencyCode.USD)
                    , "http://www.whaturlisthis.com"
                    , LineAmountType.Exclusive
                    , InvoiceStatus.Authorised
                    , InvoiceType.AccountsReceivable
                    , contact
                    , prepayments
                    , creditNotes
                    , lineItems
                    , overpayments
                    , payments
                    , dueDate
                    , total
                    , totalDiscount
                    , totalTax
                    );

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        protected void btnConnect_Click(object sender, ImageClickEventArgs e)
        {

        }

        
    }

    

    #region Documentation -----------------------------------------------------------------------------
    /// <summary>   Settings wrapper. </summary>
    ///
    /// <remarks>   Dirk Strauss, 2017/12/18. 
    ///             https://github.com/XeroAPI/Xero-Net/blob/master/Xero.Api.Example.Applications/Public/Settings.cs             
    /// </remarks>
    #endregion
    public static class Settings
    {
        public static string BaseUri
        {
            get { return ConfigurationManager.AppSettings["baseUri"]; }
        }

        //public static string CallBackUri
        //{
        //    get { return ConfigurationManager.AppSettings["callBackUri"]; }
        //}

        public static string EncryptedCertificatePath
        {
            get { return ConfigurationManager.AppSettings["certificatePath"]; }
        }

        public static string EncryptedCertificatePassword
        {
            get { return ConfigurationManager.AppSettings["certificatePassword"]; }
        }

        public static string EncryptedKey
        {
            get { return ConfigurationManager.AppSettings["consumerKey"]; }
        }

        public static string EncryptedSecret
        {
            get { return ConfigurationManager.AppSettings["consumerSecret"]; }
        }
    }
}