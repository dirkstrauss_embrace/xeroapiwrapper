﻿Xero Developer Site
---------------------------------------------------------------------------------------
https://developer.xero.com/
https://community.xero.com/developer/



Using the Xero API with C#
---------------------------------------------------------------------------------------
https://developer.xero.com/documentation/libraries/using-the-xero-api-with-c
https://github.com/XeroAPI/Xero-Net



Sample javascript for an auth popup to connect to Xero's OAuth API
---------------------------------------------------------------------------------------
https://github.com/nikz/XeroLogin



Xero API endpoint schemas
---------------------------------------------------------------------------------------
https://github.com/XeroAPI/XeroAPI-Schemas




The Authorization Flow
---------------------------------------------------------------------------------------
https://oauth1.wp-api.org/docs/basics/Auth-Flow.html




The main transactions are: 
---------------------------------------------------------------------------------------
- Customers & Contacts - (2 Way Sync) 
- Invoices (pushing into Xero predominantly) 
- Credit Notes (pushing into Xero predominantly) 
- Bank Statement Lines (pulling from Xero) 
- Once we have the basics in place we will need to look at reconciling these as Transactions 
    "Invoice Payments" are matched back to invoices 
    Expenses are processed against the chart of accounts with Rules




In terms of what the API Wrapper exposes - these are all the Entities we will interact with - some moreso that others...
---------------------------------------------------------------------------------------------------------------------------
Accounts 
- Create 
- Find *
- Update 

Bank Transactions 
- Create 
- Find * 
- Update 
 
Bank Transfers 
- Create 
- Find
 
Contacts 
- Create 
- Find *
- Update 
 
Credit Notes 
- Create 
- Find
- Update 
 
Currencies 
- Find 

Invoices 
- Create 
- Find *single
- Update 
 
Payments 
- Create  *invoice
- Find
- Update 
 
Repeating Invoices (not 100% sure whether we will need this in the API integration for AccFlow)
- Find  

Tax Rates (likely only needed when we setup the Integration, to ensure we have matching tax rates in AccFlow)
- Create 
- Find
- Update 
 
 

Possibly - still not sure : 
---------------------------------------------------------------------------------------------------------------------------
Employees 
- Create 
- Find
- Update 
 
Expense Claims 
- Create 
- Find
- Update 
  
Items 
- Create 
- Find
- Update 
  
Journals 
- Find 

Manual Journals 
- Create 
- Find
- Update 
  
Purchase Orders 
- Create 
- Find
- Update 
  
Receipts 
- Create 
- Find
- Update 
 